import 'package:clickspacetv/api.dart';
import 'package:clickspacetv/api_http.dart';


class GetProducts
{

  //=========================================
  // GETTERS
  //=========================================
  int get maxItemsPerColumn => 17;

  //=========================================
  //REQUIRED IMPLEMENTATIONS
  //===============================
  
  static List<BvItem> generateSingleTvProductList(int tvNumber, BvMonitor monitor, Function refresh) {
    List<BvItem> toReturn = <BvItem>[];
  
    if (tvNumber == 1) {
      toReturn
        ..addAll(
            monitor.flowerProductsInStock.where((e) => e.productOther == null))
        ..addAll(
            monitor.flowerProductsInStock.where((e) => e.productOther != null))
        ..addAll(monitor.prerollProductsInStock);
    } else if (tvNumber == 2) {
      toReturn
        ..addAll(monitor.edibleProductsInStock)
        ..addAll(monitor.beverageProductsInStock)
        ..addAll(monitor.oilProductsInStock)
        ..addAll(monitor.capsuleProductsInStock)
        ..addAll(monitor.topicalProductsInStock)
        ..addAll(monitor.vapeProductsInStock)
        ..addAll(monitor.concentrateProductsInStock)
        ..addAll(monitor.seedProductsInStock);
    }
    return toReturn;
  }
}
class ClientLocation {
  static const Map<int, ClientLocation> skins = {
    1: ClientLocation('BuzzedBuds-dot-Budvue', '229393'),
  };

  final String companyId;
  final String locationId;

  const ClientLocation(this.companyId, this.locationId);
}

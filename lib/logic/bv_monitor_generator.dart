import 'package:clickspacetv/api.dart';
import 'package:clickspacetv/api_http.dart';
// import 'package:clickspacetv/api.dart';
// import 'package:clickspacetv/firebase_abstract.dart';
import 'package:clickspacetv/private.dart';

import 'client_location.dart';

class BvMonitorGenerator {
  static const List<String> _categoryBlocklist = [
    "accessor",
    "clothing",
    "miscellaneous",
    "apparel",
    "merchandise",
    "shipping",
    "gift",
    "bong",
    "rolling",
    "batter",
    "lighter",
    "grinder",
    "tool",
    "rigs",
    "dugout",
    "vaporizer",
  ];
  static const Map<String, String> _nameRemapping = {};
  static const Map<String, String> _nameSplitJoin = {};
  static const Map<String, String> _nameSplitJoinStartOnly = {
    '-': '',
  };
  static const Map<String, String> _nameSplitJoinEndOnly = {
    '-': '',
  };

  static BvMonitor generate(int minStock, ClientLocation location,
      List<String> tvSpecificCategoryBlocklist) {
    BvCredentials credentials = new BvCredentials.iqmetrix(
      CstvPasskeys.IQMETRIX_KEY,
      location.companyId,
      location.locationId,
    );

    BvMonitorSettings monitorSettings = new BvMonitorSettings(
      //set to the most relevant jurisdiction - CANADA only as a last resort
      jurisdiction: BvMonitorSettings.JURISDICTION_ON,
      //true for Cova clients who are setting THC/CBD by individual packages
      useProductTests: false,
      //Monitor will print out all the categories that make it through the blocklist. Add as many as you can!!!
      categoryBlocklist: _categoryBlocklist,
      tvSpecificCategoryBlocklist: tvSpecificCategoryBlocklist,
      //The rest of the settings are mostly for BudvueTouch
    );

    BvItemParseSettings parseSettings = new BvItemParseSettings(
      sizesToMerge: BvItemParseSettings.MERGE_1G_35G_7G,
      preferWeeDb: true,
      preferWeeDbForNames: true,
      //overridden by default, try to use the POS for this.
      preferWeeDbForStrainType: false,
      //Remove Blend and/or add CBD, if needed
      preferredStrainTypes: [
        BvItem.INDICA,
        BvItem.SATIVA,
        BvItem.HYBRID,
        BvItem.CBD
      ],
      //Especially with Cannabis 2.0, this should almost always be true
      preferWeeDbForFormat: true,
      //If the client *never* wants THC/CBD from the province, set this to false.
      useWeeDbForFallbackThcCbd: true,
      //set via text1, typically
      minimumStock: minStock ?? 1,
      //Set to true to normalize all mg/g measurements to percentages
      convertMgGToPercent: true,
      //For 1:1 overwrites of specific product names. Case sensitive
      nameRemapping: _nameRemapping,
      //For replacing redundant strings (like "PreRoll"). Not case sensitive
      nameSplitJoin: _nameSplitJoin,
      //For replacing redundant strings at the start of names (like "Flower - "). Not case sensitive
      nameSplitJoinStartOnly: _nameSplitJoinStartOnly,
      //For replacing redundant strings at the end of names (like " - 1g"). Not case sensitive
      nameSplitJoinEndOnly: _nameSplitJoinEndOnly,
      preParseHook: _preParseHook,
    );

    BvMonitor monitor = new BvMonitor(
      credentials: credentials,
      monitorSettings: monitorSettings,
      parseSettings: parseSettings,
    );

    return monitor;
  }

  static void _preParseHook(BvItem item) {
  }
}

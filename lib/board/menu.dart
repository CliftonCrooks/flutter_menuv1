import 'dart:async';
import 'package:clickspacetv/api_http.dart';
import 'package:clickspacetv/api.dart';
import 'package:clickspacetv/slide.dart';
import 'package:menu_flutter/animations/animated_background.dart';
import 'package:menu_flutter/board/item.dart';
import 'package:flutter/material.dart';
import 'package:menu_flutter/get_products.dart';
import 'board_widgets.dart';

class MenuScreen extends StatefulWidget {
  final BvMonitor monitor;
  final int tvNumber;
  MenuScreen(this.monitor, this.tvNumber);
  @override
  _MenuScreenState createState() => _MenuScreenState();
}

class _MenuScreenState extends State<MenuScreen> {

  //Initialize timer, to wait on the monitor to get ready.
  //loading screen while waiting.
  //If the monitor is ready, then start to build
  //handle checking if the monitor was initialized before checking if it is ready
  List<String> images = ["assets/1.png", "assets/2.png", "assets/3.png"];
  List<BvItem> items = [];

  ScrollController _controller;
  @override
  void initState() {
    //Budvue logic
    CommunicationManager.sendMessageAllowRunInfinite(true);

    _controller = new ScrollController();
    super.initState();
    Timer.periodic(Duration(seconds: 15), (_) {
      if (mounted) {
        setState(() {
          if (_controller.offset == _controller.position.maxScrollExtent) {
            _controller.animateTo(_controller.position.minScrollExtent,
                duration: Duration(seconds: 12), curve: Curves.linear);
          } else {
            _controller.animateTo(_controller.position.maxScrollExtent,
                duration: Duration(seconds: 12), curve: Curves.easeIn);
          }
          print("This is the length of items ${items.length}");
        });
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    items = GetProducts.generateSingleTvProductList(1, widget.monitor, ()=>setState((){print("refresh called");}));
  }

  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        width: _size.width,
        height: _size.height,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/img/frameOneH1.jpg"))),
        child: AnimatedBackground(
          playAnimation: false,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(48, 16, 8, 12),
                child: Header(),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 12, 8, 1),
                child: Headings(),
              ),
              Divider(
                color: Colors.white,
                thickness: 0.5,
              ),
              Expanded(
                  child: ListView(
                      controller: _controller,
                      children: items.map((BvItem item) {
                        return MenuItem(
                          product: item?.productName ?? "",
                          image: images[0],
                          thc: item?.thcAvg ?? 0.0,
                          chd: item?.cbdAvg ?? 0.0,
                          producer: item?.brandName ?? "",
                          size: item?.getNetSizeSummaryStyled()??"",
                          price: item?.currentPrice ?? " ",
                          additional: 'NEW' ,
                        );
                      }).toList()))
            ],
          ),
        ),
      ),
    );
  }
}

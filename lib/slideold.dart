// import 'dart:math';
//
// import 'package:clickspacetv/api_http.dart';
// import 'package:clickspacetv/slide.dart';
//
// import 'display/client_menu_field.dart';
// import 'display/main_display.dart';
// import 'logic/bv_monitor_generator.dart';
// import 'logic/client_location.dart';
//
// void main() {
//   new _BuzzedBudsMenuV1();
// }
//
// class _BuzzedBudsMenuV1 extends CstvSlide {
//   //=========================================
//   // VARS
//   //===============================
//   //Monitor
//   BvMonitor _monitor;
//   //Display Elements
//   MainDisplay mainDisplay;
//
//   //=========================================
//   // CONSTRUCTOR / INIT
//   //===============================
//   _BuzzedBudsMenuV1() {
//     setOrientation(CstvSlide.ORIENTATION_VERTICAL);
//   }
//
//   //=========================================
//   // OVERRIDES
//   //===============================
//   void debugSetup() {
//     int totalSkins = ClientLocation.skins.length;
//     int totalTvs = 2;
//     AppData.isInFrameMode = false;
//     Data.slideSkin = (new DateTime.now().second % totalSkins) + 1;
//     Data.text1 = '1';
//     Data.text2 = '5';
//     Data.text3 = (new Random().nextInt(totalTvs) + 1).toString();
//     Data.slideDuration = 3600;
//     Data.fileVideos.add(
//         new DrupalFileReference.fromDebugData("BV Buzzed Cannabis BG.webm"));
//     print(
//         'Running slideSkin ${Data.slideSkin} with minStock ${Data.text1}, transitionSpeed ${Data.text2}, TV# ${Data.text3} of ${totalTvs}');
//   }
//
//   bool onInitLoopIsSlideDataComplete() {
//     if (Data.fileVideos.length != 1) return false;
//     if (Data.slideSkin == null ||
//         Data.slideSkin < 1 ||
//         Data.slideSkin > ClientLocation.skins.length) return false;
//
//     int tvNumber = int.tryParse(Data.text3.toString());
//     int totalTvs = 2;
//     if (tvNumber == null || totalTvs == null || tvNumber > totalTvs)
//       return false;
//
//     if (_monitor == null) {
//       //Fallback self-destruct
//       launchSelfDestructTimer();
//
//       //parse data - i.e. swap times, stock levels, etc
//       int minStock = int.tryParse(Data.text1.toString());
//       int transitionSpeed = int.tryParse(Data.text2.toString());
//
//       if (transitionSpeed != null &&
//           transitionSpeed > 2 &&
//           transitionSpeed < 500) {
//         ClientMenuField.scrollSpeedPxPerSec = transitionSpeed;
//         ClientMenuField.pagedTransitionSpeedSec = transitionSpeed;
//       }
//
//       // Set up the tvSpecificCategoryBlocklist. Case insensitve, and partial
//       // matches will be blocked. For example, if TV#1 doesn't need info about
//       // the "flower" category, add "flower" here and this TV won't ever
//       // download those products. Only blocks on POS categpries, not format
//       // or strain type, so consult the console log on first run to get the
//       // list of categories that are being listened to.
//       List<String> tvSpecificCategoryBlocklist = [];
//
//       //setup the monitor
//       _monitor = BvMonitorGenerator.generate(
//         minStock,
//         ClientLocation.skins[Data.slideSkin],
//         tvSpecificCategoryBlocklist,
//       );
//     }
//     if (_monitor.isInitialized == false) {
//       return false;
//     }
//
//     //monitor may not be ready, but it's at least initialized. Continue with display logic.
//     return true;
//   }
//
//   void selfDestructTimerHandler() {
//     if (_monitor == null || _monitor.isReady == false) {
//       CommunicationManager.sendMessageSelfDestruct('_monitor not ready');
//     } else if (mainDisplay == null) {
//       CommunicationManager.sendMessageSelfDestruct('cstvDisplay not ready');
//     }
//   }
//
//   bool onInitLoopIsSlideReady() {
//     if (AppData.isInFrameMode == true) {
//       throw new ArgumentError(
//           'Error: AppData.isInFrameMode == true, but this slide does not support FrameMode.');
//     } else {
//       int tvNumber = int.tryParse(Data.text3.toString());
//       mainDisplay = new MainDisplay(display, _monitor, tvNumber);
//     }
//
//     return true;
//   }
//
//   bool doesSlideWantToPlay() {
//     return true;
//   }
//
//   void onPlaySlide() {
//     mainDisplay.play();
//   }
//
//   void onStopSlide() {
//     mainDisplay.stop();
//   }
//
//   void onKillSlide() {}
// }
